import { cloudServiceName } from '../utils/global'

import request from '../utils/request'

export default {
    login: function (userInfo) {
        return request({
            method: "post",
            data: userInfo,
            url: cloudServiceName.securityService + "/rest/pb/user/login"
        })
    }
}