import {cloudServiceName} from '../utils/global'

import request from '../utils/request'

export default {
    findAll:function () {
        return request({
            method:'get',
            url:cloudServiceName.newsService + '/rest/pb/news',
        })
    },
    add:function (news) {
        return request({
            method:'post',
            data:news,
            url:cloudServiceName.newsService + '/rest/pt/news'
        })
    },
    findById:function (id) {
        return request({
            method:'get',
            url:cloudServiceName.newsService + '/rest/pb/news/'+id,
        })
    },
    /**
     *
     * @param queryProps 分页查询参数 pageCount pageSize moduleId
     * @param successCallback
     * @param errorCallback
     */
    findByPage:function (queryParams) {
        //http://localhost:10000/hnister-news-service/pb/news/@/for=page?pageCount=1&pageSize=2&moduleId=3
        return request({
            method:'get',
            params:queryParams,
            url:cloudServiceName.newsService + '/rest/pb/news/@/for=page'
        })
    },
    deleteById:function(id){
        return request({
            method:'delete',
            url:cloudServiceName.newsService + '/rest/pt/news/'+id,
        })
    },
    edit:function (news) {
        return request({
            method:'put',
            data:news,
            url:cloudServiceName.newsService + '/rest/pt/news'
        })
    }
}
