import accountApi from '../../api/account'
import loginEnum from '../../enums/loginEnum'

import Vue from 'vue'

const state = {
    login:{}
}

const getters = {
    Login: (state) =>{
        return state.login;
    }
}

//只进行同步功能 异步操作在action中进行
const mutations = {
    headMenuTree:(state,login)=>{
        Vue.set(state,loginEnum.login,login)
    }
}

const actions = {
    Login:({commit}, userInfo) =>{
        accountApi.login(userInfo)
            .then(response =>{
                // 登录成功/失败
                const data = response
                if (!data.success){
                    Message.error(data.msg)
                   // TODO
                    return
                }

                var token = data.msg;
            }).catch(error =>{
                console.log(error)
            })
    }
}

export default{
    state,
    getters,
    mutations,
    actions
}