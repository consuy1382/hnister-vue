//定义全局变量
const address = {
    zuul:'http://zuul.hnister.cn:10000',
    //七牛云的华南区文件上传地址
    qiniuUploadURL:'http://up-z2.qiniu.com',
    //我的七牛云图片下载host
    qiniuDownloadURL:'http://p4fkxpg80.bkt.clouddn.com',
}
const httpMethodMap = {
    '0':'GET',
    '1':'HEAD',
    '2':'POST',
    '3':'PUT',
    '4':'PATCH',
    '5':'DELETE',
    '6':'OPTIONS',
    '7':'TRACE',
}

const resourceStatusMap = {
    '0':'开放',
    '1':'监控',
    '2':'禁用',
}

const userTypeMap = {
    '0':'普通',
    '1':'管理员',
}
const userStatusMap = {
    '0':'未验证',
    '1':'已验证'
}
const userGenderMap = {
    '0':'女',
    '1':'男',
}

//服务名称
const cloudServiceName = {
    settingService:'hnister-setting-service',
    newsService:'hnister-news-service',
    securityService:'hnister-security-service',
}

export {
    address,
    httpMethodMap,
    resourceStatusMap,
    userTypeMap,
    userStatusMap,
    userGenderMap,
    cloudServiceName
}


